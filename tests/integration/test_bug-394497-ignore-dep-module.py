# Verify that --ignore-modules works for modules that would be included with
# --include-dependencies in effect.
# See bug 394497 -- https://bugs.kde.org/show_bug.cgi?id=394497

import unittest
from ksblib.Application import Application

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


# Redefine ksb::Application::_resolveModuleDependencies to avoid requiring metadata
# module.
class Application_Test(Application):
    def _resolveModuleDependencyGraph(self, modules: list):
        newModule = self.module_factory("setmod2")
        
        graph = {}
        
        # Construct graph manually based on real module list
        for module in modules:
            name = module.name
            graph[name] = {
                "votes": {},
                "build": 1,
                "module": module,
            }
        
        if "setmod1" in graph:
            graph["setmod1"]["votes"] = {
                "setmod2": 1,
                "setmod3": 1
            }
            
            # setmod1 is only user of setmod2
            if "setmod2" not in graph:
                graph["setmod2"] = {
                    "votes": {
                        "setmod3": 1
                    },
                    "build": 1,
                    "module": newModule,
                }
        
        return {"graph": graph}


class TestApp(unittest.TestCase):
    def setUp(self):
        self.args = "--pretend --rc-file tests/integration/fixtures/sample-rc/kdesrc-buildrc --include-dependencies setmod1 setmod3".split(" ")

    def test1(self):
        app = Application_Test(self.args)
        moduleList = app.modules
        
        self.assertIs(len(moduleList), 3, "Right number of modules (include-dependencies)")
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        self.assertEqual(moduleList[1].name, "setmod2", "mod list[1] == setmod2")
        self.assertEqual(moduleList[2].name, "setmod3", "mod list[2] == setmod3")

    def test2(self):
        self.args.extend(["--ignore-modules", "setmod2"])
        app = Application_Test(self.args)
        moduleList = app.modules
        
        self.assertIs(len(moduleList), 2, "Right number of modules (include-dependencies+ignore-modules)")
        self.assertEqual(moduleList[0].name, "setmod1", "mod list[0] == setmod1")
        self.assertEqual(moduleList[1].name, "setmod3", "mod list[1] == setmod3")
    
    # Verify that --include-dependencies on a moduleset name filters out the whole set
    def test3(self):
        self.args = self.args[:3] + ["--ignore-modules", "set1"]
        
        app = Application_Test(self.args)
        moduleList = app.modules
        
        self.assertIs(len(moduleList), 1, "Right number of modules (ignore module-set)")
        self.assertEqual(moduleList[0].name, "module2", "mod list[0] == module2")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
