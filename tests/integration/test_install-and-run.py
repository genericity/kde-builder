# Test install and ability to run a simple status command w/out Perl failures

import os
import subprocess
import unittest
import tempfile
import re
# use Test::More;
# use POSIX;
# use File::Basename;
#
# use Cwd;
# use IPC::Cmd;


# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>

class TestApp(unittest.TestCase):
    def test1(self):
        # Assume we're running directly for git source root, as required for rest of
        # test suite.
        
        self.assertTrue(os.path.isdir("tests"), "Test directory in right spot")
        self.assertTrue(os.path.isfile("kde-builder"), "kde-builder script in right spot")
        
        # This test can't pass from an installed kdesrc-build, unless user goes out of
        # their way to move files around or establish a broken module layout. If this
        # passes, we should be able to assume we're running from a git source dir
        self.assertTrue(os.path.isfile("ksblib/Version.py"), "kde-builder modules found in git-src")
        
        # Make sure kdesrc-build can still at least start when run directly
        result = subprocess.run(["./kde-builder", "--version", "--pretend"])
        self.assertEqual(result.returncode, 0, "Direct-run kde-builder works")

    def test2(self):
        print("Installing kdesrc-build to simulate running from install-dir")
        
        with tempfile.TemporaryDirectory() as tempInstallDir:
            os.mkdir(f"{tempInstallDir}/bin")
            
            curdir = os.getcwd()
            os.symlink(f"{curdir}/kde-builder", f"{tempInstallDir}/bin/kde-builder")
            
            # Ensure a direct symlink to the source directory of kdesrc-build still works
            os.environ["PATH"] = f"{tempInstallDir}/bin:" + os.environ.get("PATH")
        
            output = subprocess.check_output("pipenv run python kde-builder --version --pretend".split(" ")).decode().strip()
            self.assertTrue(re.match(r"^kde-builder \d\d\.\d\d \(v\d\d", output), "--version for git-based version is appropriate")
            self.assertTrue(os.path.islink(f"{tempInstallDir}/bin/kde-builder"), "kdesrc-build is supposed to be a symlink")
            self.assertEqual(os.unlink(f"{tempInstallDir}/bin/kde-builder"), None, "Remove kdesrc-build symlink, so it will not conflict with install")
            
            # pl2py: We do not need to install kde-builder to the install-dir. Users will run it from source dir
            # # Ensure the installed version also works.
            # # TODO: Use manipulation on installed ksb::Version to ensure we're seeing right
            # # output?
            #
            # with tempfile.TemporaryDirectory() as tempBuildDir:
            #     os.chdir(f"{tempBuildDir}")
            #
            #     # Use IPC::Cmd to capture (and ignore) output. All we need is the exit code
            #
            #     command = ["cmake", f"-DCMAKE_INSTALL_PREFIX={tempInstallDir}", "-DBUILD_doc=OFF", curdir]
            #     buildResult = subprocess.run(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=60).returncode
            #
            #     if not buildResult:
            #         raise "Couldn't run cmake!"
            #
            #     buildResult = subprocess.run(["make"]).returncode
            #
            #     if buildResult == -1 or buildResult != 0:
            #         raise f"Couldn't run make! {buildResult}"
            #
            #     buildResult = subprocess.run(["make", "install"]).returncode
            #
            #     if buildResult == -1 or buildResult != 0:
            #         raise f"Couldn't install! {buildResult}"
            #
            #     # Ensure newly-installed version is first in PATH
            #     os.environ["PATH"] = f"{tempInstallDir}/bin:" + os.environ.get("PATH")
            #
            #     # # Ensure we don't accidentally use the git repo modules/ path when we need to use
            #     # # installed or system Perl modules
            #     # local $ENV{PERL5LIB}; # prove turns -Ilib into an env setting
            #
            #     output = subprocess.run("kde-builder --version --pretend").stdout.decode()
            #     self.assertTrue(re.match(r"^kde-builder \d\d\.\d\d\n?$", output), "--version for installed version is appropriate")
            #     os.chdir(curdir)

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
