# Loads every single ksb module to make sure they all compile.

# use ksb;
# use Test::More import => ['!note'];
# use POSIX;
# use File::Basename;
import unittest

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>


class TestApp(unittest.TestCase):
    def setUp(self):
        import ksblib.Application
        import ksblib.BuildContext
        import ksblib.BuildException
        import ksblib.BuildSystem
        import ksblib.BuildSystem.Autotools
        import ksblib.BuildSystem.CMakeBootstrap
        import ksblib.BuildSystem.KDECMake
        import ksblib.BuildSystem.Meson
        import ksblib.BuildSystem.QMake
        import ksblib.BuildSystem.QMake6
        import ksblib.BuildSystem.Qt4
        import ksblib.BuildSystem.Qt5
        import ksblib.BuildSystem.Qt6
        import ksblib.Cmdline
        import ksblib.Debug
        import ksblib.DebugOrderHints
        import ksblib.DependencyResolver
        import ksblib.FirstRun
        import ksblib.IPC
        import ksblib.IPC.Null
        import ksblib.IPC.Pipe
        import ksblib.KDEProjectsReader
        import ksblib.Module
        import ksblib.Module.BranchGroupResolver
        import ksblib.ModuleResolver
        import ksblib.ModuleSet
        import ksblib.ModuleSet.KDEProjects
        import ksblib.ModuleSet.Null
        import ksblib.ModuleSet.Qt
        import ksblib.OptionsBase
        import ksblib.OSSupport
        import ksblib.PhaseList
        import ksblib.RecursiveFH
        import ksblib.StatusView
        import ksblib.TaskManager
        import ksblib.Updater
        import ksblib.Updater.Git
        import ksblib.Updater.KDEProject
        import ksblib.Updater.KDEProjectMetadata
        import ksblib.Updater.Qt5
        import ksblib.Util
        import ksblib.Util.LoggedSubprocess
        import ksblib.Version
    
    def test1(self):
        self.assertTrue(1 == 1, "Able to compile and load all kdesrc-build modules.")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
