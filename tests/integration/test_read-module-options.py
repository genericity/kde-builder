# Test basic option reading from rc-files
import os
import unittest
from ksblib.Util.Util import Util  # load early so we can override
from promise import Promise

# use Test::More;
# use Carp qw(confess);
# use POSIX;
# use File::Basename;

# # <editor-fold desc="Begin collapsible section">
# my $timestamp1 = POSIX::strftime("%s", localtime);
# my $filename = basename(__FILE__);
# my $section_header = "File: $filename (click to toggle collapse)";
# print "\e[0Ksection_start:${timestamp1}:$filename\[collapsed=true]\r\e[0K$section_header\n";  # displayed in collapsible section in gitlab ci job log
# # </editor-fold>

# Now we can load ksb::Application, which will load a bunch more modules all
# using log_command and run_logged_p from ksb::Util
from ksblib.Application import Application
from ksblib.Updater.Git import Updater_Git


class TestApp(unittest.TestCase):
    
    def setUp(self):
        # Override ksb::Util::log_command for final test to see if it is called with
        # 'cmake'
        
        self.CMD = []
        
        def mock_run_logged_p(module, filename, directory, argRef):
            if not argRef:
                raise "No arg to module"
            command = argRef
            if "cmake" in command:
                self.CMD = command
            
            return Promise.resolve(0)
        
        Util.run_logged_p = mock_run_logged_p
        
    def test1(self):
        
        app = Application("--pretend --rc-file tests/integration/fixtures/sample-rc/kdesrc-buildrc".split(" "))
        moduleList = app.modules
        
        self.assertEqual(len(moduleList), 4, "Right number of modules")
        
        # module2 is last in rc-file so should sort last
        self.assertEqual(moduleList[3].name, "module2", "Right module name")
        
        scm = moduleList[3].scm()
        self.assertIsInstance(scm, Updater_Git)
        
        branch, sourcetype = scm._determinePreferredCheckoutSource()
        
        self.assertEqual(branch, "refs/tags/fake-tag5", "Right tag name")
        self.assertEqual(sourcetype, "tag", "Result came back as a tag")
        
        # setmod2 is second module in set of 3 at start, should be second overall
        self.assertEqual(moduleList[1].name, "setmod2", "Right module name from module-set")
        branch, sourcetype = moduleList[1].scm()._determinePreferredCheckoutSource()
        
        self.assertEqual(branch, "refs/tags/tag-setmod2", "Right tag name (options block)")
        self.assertEqual(sourcetype, "tag", "options block came back as tag")
        
        # Test some of the option parsing indirectly by seeing how the value is input
        # into build system.
        
        # Override auto-detection since no source is downloaded
        moduleList[1].setOption({"override-build-system": "kde"})
        
        # Should do nothing in --pretend
        self.assertTrue(moduleList[1].setupBuildSystem(), 'setup fake build system')
        
        self.assertTrue(self.CMD, "run_logged_p cmake was called")
        self.assertEqual(len(self.CMD), 12)
        
        self.assertEqual(self.CMD[0], "cmake", "CMake command should start with cmake")
        self.assertEqual(self.CMD[1], "-B",    "Passed build dir to cmake")
        self.assertEqual(self.CMD[2], ".",     "Passed cur dir as build dir to cmake")
        self.assertEqual(self.CMD[3], "-S",    "Pass source dir to cmake")
        self.assertEqual(self.CMD[4], "/tmp/setmod2", "CMake command should specify source directory after -S")
        self.assertEqual(self.CMD[5], "-G", "CMake generator should be specified explicitly")
        self.assertEqual(self.CMD[6], "Unix Makefiles", "Expect the default CMake generator to be used")
        self.assertEqual(self.CMD[7], "-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON", "Per default we generate compile_commands.json")
        self.assertEqual(self.CMD[8], "-DCMAKE_BUILD_TYPE=a b", "CMake options can be quoted")
        self.assertEqual(self.CMD[9], "bar=c", "CMake option quoting does not eat all options")
        self.assertEqual(self.CMD[10], "baz", "Plain CMake options are preserved correctly")
        self.assertEqual(self.CMD[11], f"-DCMAKE_INSTALL_PREFIX={os.environ.get("HOME")}/kde/usr", "Prefix is passed to cmake")
        
        # See https://phabricator.kde.org/D18165
        self.assertEqual(moduleList[0].getOption("cxxflags"), "", "empty cxxflags renders with no whitespace in module")

# # <editor-fold desc="End collapsible section">
# my $timestamp2 = POSIX::strftime("%s", localtime);
# print "\e[0Ksection_end:${timestamp2}:$filename\r\e[0K\n";  # close collapsible section
# # </editor-fold>
