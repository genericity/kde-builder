# KDE Builder

This script streamlines the process of setting up and maintaining a development
environment for KDE software.

It does this by automating the process of downloading source code from the
KDE source code repositories, building that source code, and installing it
to your local system.

### The goal of the project

**KDE Builder** is a drop-in replacement for the **Kdesrc Build** project. It is the exact reimplementation
of the predecessor script, but in Python - a much more acknowledged language. The original project is in Perl,
and this is a significant barrier for new contributions.

After switching to this project, those much wanted features (see the bugtracker) can be implemented with ease.

## Testing welcome (February 2024)

This project is not yet "officially" released. Current state is "should work", but
there are some things that needs to be finished:

- Unit and Integration tests needs to be made automated with gitlab ci
- DBus module needs to be ported
- Todo comments in the project needs to be handled
- Real life testing of project

You can help with the last point. Follow the installation procedure, then try
to use `kde-builder` the same as you normally run `kesrc-build`.

If you found something not working in `kde-builder` that works in `kdesrc-build`,
please report it. You can write in the matrix chat (see [here](https://community.kde.org/Get_Involved/development#Where_to_find_the_development_team))
or creating an issue in the [project repo](https://invent.kde.org/ashark/kde-builder).

### Updating your clone

Currently, the project history is periodically overwritten by force-pushing to master. This way the project will have
clean history at the moment of "official launching" (presumably at the 28 February 2024, the Mega 
Release date).

So, if you have cloned this repo, and then want to pull new updates, you can do the following:

```bash
git pull --rebase
```

If instead you have forked this repo, and you need to rebase, you will need to do the following:

```bash
git remote add upstream https://invent.kde.org/ashark/kde-builder.git
git pull --rebase --strategy-option=ours upstream master
```

## Installation

This project targets Python version 3.12.

Install Python 3.12:

* Arch: `yay -S python312`
* Fedora 39: `sudo dnf install python3.12`
* openSUSE Tumbleweed: `sudo zypper install python312`
* Debian/Ubuntu: `sudo apt install python3.12`

Install pipenv.

* Arch: `sudo pacman -S python-pipenv`
* Fedora 39: `sudo dnf install python3-pipenv`
* openSUSE Tumbleweed: not available in the repositories. You will need to install it with `pip install`.
* Debian/Ubuntu: `sudo apt install pipenv`

Open the terminal in the directory where you would store the project and clone it:

```bash
mkdir -p ~/kde/src
cd ~/kde/src
git clone git@invent.kde.org:ashark/kde-builder.git
cd kde-builder
```

Create a virtual environment with the required packages:

```bash
pipenv install
```

To be able to invoke the script by just its name, create a wrapper script.  
Create a file `~/bin/kde-builder` (assuming the `~/bin` is in your `PATH`), make this file executable.  
Add the following content to it:

```bash
#!/bin/bash

cd ~/kde/src/kde-builder
pipenv run python kde-builder $@
```

Follow the initial setup procedure. It will install the required packages, create the configuration file
and update your shell configuration file:

```bash
kde-builder --initial-setup
```

Now you can observe the build plan:

```bash
kde-builder --pretend kcalc
```

## Usage

Build a project and its dependencies:

```bash
kde-builder kcalc
```

Rebuild only a single project without updating the source code:

```bash
kde-builder --no-include-dependencies --no-src kcalc
```

Launch the binary for a project using the development environment:

```bash
kde-builder-launch kcalc
```

To build a specific project while skipping certain modules:

```bash
kde-builder kcalc --ignore-modules kxmlgui
```

## Documentation

See the wiki page [Get_Involved/development](https://community.kde.org/Get_Involved/development).
It covers usage of `kdesrc-build` script - a perl predecessor of `kde-builder`.

For more details, consult the project documentation. The most important pages are:

- [List of supported configuration options](https://docs.kde.org/trunk5/en/kdesrc-build/kdesrc-build/conf-options-table.html)
- [Supported command line parameters](https://docs.kde.org/trunk5/en/kdesrc-build/kdesrc-build/supported-cmdline-params.html)
